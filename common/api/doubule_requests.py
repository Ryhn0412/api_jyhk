import json
import os
import re

import allure
from jsonpath import jsonpath

from common.api.requests import request_ryhn
from utill.create_util.yaml_util import yaml_util


class double_requests(request_ryhn):
    '''
    关于接口请求的二次封装
    '''
    def get_data(self, caseinfo):
        '''
        请求前获取入参内需要进行参数化的数据，然后进行处理
        :return: 数据处理，返回参数化的数据
        '''
        # 获取入参的最初数据
        data = caseinfo['request']['data']
        # 查找需要进行参数化的入参数据
        re_data = re.findall("[$][a-zA-Z]+", str(data))
        # 进行判断查看是否需要进行参数化
        if len(re_data) != 0:
            # 获取正则匹配下来的需要参数化数据处理
            num = [bc.split("$")[1] for bc in re_data]
            # 获取环境变量里关于需要进行参数化的返回信息
            num_list = [os.environ[li.upper()] for li in num]
            # 进行数据合并，形成字典格式
            a = [k for k in dict(zip(re_data, num_list)).items()]
            # 进行参数化替换
            for cd in a:
                data = str(data).replace(cd[0], cd[1])
                # print(type(data))
            return eval(data)
        else:
            return caseinfo['request']['data']
    def complex_data(self, caseinfo, value):
        '''
        分析返回信息的数据，并参数化
        :return:
        '''
        if caseinfo['response']['data'] != None:
            link_list = [(k, v) for k, v in caseinfo['response']['data'].items()]
            if caseinfo['exceptdata']['except_data']['select'] =='re':
                for link_value in link_list:
                    key = link_value[0]
                    os.environ[key] = re.search(link_value[1], value.text).group()
            elif caseinfo['exceptdata']['except_data']['select'] =='jsonpath':
                for link_value in link_list:
                    key = link_value[0]
                    print(str(jsonpath(value.json(), link_value[1])[0]))
                    os.environ[key] = str(jsonpath(value.json(), link_value[1])[0])

    def ryhn_request(self, caseinfo, data):
        '''
        对send_request方法二次封装(读取yaml里的所有参数，并自行添加额外参数）
        :return:
        '''
        with allure.step("接口请求"):
            method = caseinfo['request']['method']
            url = caseinfo['request']['url']
            allure.attach(
                body="{request}".format(request=data),
                name="请求参数",
                attachment_type=allure.attachment_type.TEXT)
            return request_ryhn().send_value(
                method=method, url=url, data=data,headers=yaml_util().read_yaml(
                    yaml_util().yaml_url(r'\yaml_package\header.yaml')
                ), timeout=4.0)

    def ryhn_request_C(self, caseinfo, data):
        '''
        对send_request方法二次封装(读取yaml里的所有参数，并自行添加额外参数）
        :return:
        '''
        with allure.step("接口请求"):
            method = caseinfo['request']['method']
            url = caseinfo['request']['url']
            allure.attach(
                body="{request}".format(request=data),
                name="请求参数",
                attachment_type=allure.attachment_type.TEXT)
            return request_ryhn().send_value(
                method=method, url=url, data=data,headers=yaml_util().read_yaml(
                    yaml_util().yaml_url(r'\yaml_package\header_C.yaml')
                ), timeout=4.0)

    def ryhn_request_M(self, caseinfo, data):
        '''
        对send_request方法二次封装(读取yaml里的所有参数，并自行添加额外参数）
        :return:
        '''
        with allure.step("接口请求"):
            method = caseinfo['request']['method']
            url = caseinfo['request']['url']
            allure.attach(
                body="{request}".format(request=data),
                name="请求参数",
                attachment_type=allure.attachment_type.TEXT)
            return request_ryhn().send_value(
                method=method, url=url, data=data,headers=yaml_util().read_yaml(
                    yaml_util().yaml_url(r'\yaml_package\header_M.yaml')
                ), timeout=4.0)