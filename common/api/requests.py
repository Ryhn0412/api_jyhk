import json
import requests


class request_ryhn:
    '''
    有关request的代码封装
    '''
    session = requests.Session()

    def send_value(self, method, url, data, **kwargs):
        '''
        接口请求方法的核心封装
        :param method: 请求方式
        :param url: 请求路径
        :param data: 请求数据
        :param kwargs: 其它需要的参数
        :return: 请求返回结果
        '''
        method = str(method).lower()
        if method == 'get':
            return request_ryhn.session.request(
                method, url=url, params=data,**kwargs)
        else:
            data = json.dumps(data)
            return request_ryhn.session.request(
                method, url=url, data=data,**kwargs)
