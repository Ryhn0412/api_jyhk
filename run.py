# import requests

# # r = requests.request()
# # # 返回字符串的数据
# # print(r.text)
# # # 返回字节格式的数据
# # print(r.content)
# # # 返回字典格式的数据
# # print(r.json())
# # # 状态码
# # print(r.status_code)
# # # 返回状态信息
# # print(r.reason)
# # # 返回cookie信息
# # print(r.cookies)
# # # 返回编码格式
# # print(r.encoding)
# # # 返回响应头信息
# # print(r.headers)"

import os
from turtle import pd

import pytest
import xlrd
import yaml

from utill.create_util.yaml_util import yaml_util
from utill.request_util.yaml_data import render, all_functions
from utill.response_util.jira_util import jira_util
from utill.response_util.email_util import email_send


if __name__ == '__main__':
    pytest.main(['-vs', '--alluredir', './report/result/'])
    # 生成报告文件
    os.system('allure generate ./report/result/ -o ./report/allure --clean')
    # 登记问题至jira上
    jira_util().add_issue()
    # 自动统计jira上的问题并整理成图表发送至邮箱
    email_send()
