import datetime
import queue
import random
import re
import threading
import time
import traceback
from multiprocessing import Process

from functools import wraps

from common.api.requests import request_ryhn
from utill.create_util.yaml_util import yaml_util
from utill.random_util.random_idcard import id_card
from utill.random_util.random_phone import get_phone_num
from utill.request_util.secretykey_util import base641, Sha256


def account_time(func):
    def time_account(*args, **kwargs):
        start = datetime.datetime.now()
        end = datetime.datetime.now()
        print('time:{time}'.format(time=end - start))

    return time_account


class api_data(object):
    '''
    用于造数据时候做的脚本
    '''
    @staticmethod
    def accesstoken():
        '''
        获取accesstoken
        :return:
        '''
        a = base641(account="otsp-manage-sys-web")
        b = Sha256(
            secret="f9xcfnqmzv710jf84ft59afc61mpqa8q",
            key="acfe81hfcn91zmax")
        headers = {
            'apptoken': '{result}'.format(
                result=a + '.' + b),
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit"
            "/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36 Edg/100.0.1185.50",
            "Content-Type": "application/json;charset=UTF-8"}
        url = r'https://hlwyy.szmbzx.com/test/otsp-manage-sys/japi/v1/manage/login'
        method = 'POST'
        data = {
            "loginName": "jyhk",
            "loginPwd": "Kingtang=2020",
            "manCaptcha": {
                "captchaKey": "string",
                "captchaValue": "string"
            },
            "type": 0
        }
        value = request_ryhn().send_value(
            method=method, url=url, data=data, headers=headers)
        r = value.json()
        print(r)
        # 重新编写请求头信息
        headers1 = {
            "accessToken": r['accessToken'],
            "Content-Type": headers['Content-Type'],
            "User-Agent": headers['User-Agent'],
            "appToken": headers['apptoken']
        }
        return headers1

    def add_hospital(self, headers):
        '''
        添加医院主体
        :return:
        '''
        url = r'https://hlwyy.szmbzx.com/test/otsp-manage-sys/japi/v1/manage/organization-save'
        method = 'POST'
        data = {
            "code": str(random.randint(1, 99999)),
            "name": "测试" + random.choice(['医院', '卫生院', '机构', '集团']) + str(random.randint(1, 99999)),
            "type1Id": "308",
            "type2Id": "314",
            "detailAddress": "测试",
            "responsibleName": "测试" + str(random.randint(1, 999)),
            "contactName": "ryhn" + str(random.randint(1, 99)),
            "responsiblePhone": get_phone_num(),
            "contactPhone": get_phone_num(),
            "organizationCodeCertificate": str(random.randint(1, 99999)),
            "parentId": "1",
            "provinceAreaId": "2",
            "cityAreaId": "3",
            "countyAreaId": 0,
            "areaId": "3",
            "latitude": "",
            "longitude": ""
        }
        value = request_ryhn().send_value(method, url, data, headers=headers)
        print(value.json())
        return value.json()['oid']



    def get_entity(self, oid, header):
        '''
        获取二级机构id
        :return:
        '''
        url = "https://www.online-medical.com.cn//otsp-manage-sys/japi/v2/manage/org/entity/page"
        method = "GET"
        data = {
            "oid": oid
        }
        value = request_ryhn().send_value(method, url, data, headers=header)
        print(value.json()['data'][0]['id'])
        return value.json()['data'][0]['id']

    def hospital_service(self, oid, header):
        '''
        开通医院服务
        :return:
        '''
        url = 'https://www.online-medical.com.cn//otsp-service/japi/v1/service/organization_service-rel'
        method = 'POST'
        data = {
            "activateStatus": 1,
            "oid": oid,
            "serviceId": 12,
            "status": 1
        }
        value = request_ryhn().send_value(method, url, data, headers=header)
        data['serviceId'] = 14
        value = request_ryhn().send_value(method, url, data, headers=header)

    def add_entity(self, oid, headers):
        '''
        生成医院下的二级机构
        :return:
        '''

        url = 'https://www.online-medical.com.cn//otsp-manage-sys/japi/v1/manage/organization-entity-save'
        method = 'POST'
        data = {
            "code": str(random.randint(1, 99999)),
            "shortCode": str(random.randint(1, 999)),
            "name": "测试" + random.choice(['公司', '集团', '药店']) + str(random.randint(1, 99)),
            "typeId": "508",
            "detailAddress": "测试地址" + str(random.randint(1, 99)),
            "responsibleName": "测试人员" + str(random.randint(1, 99)),
            "contactName": "测试员" + str(random.randint(1, 99)),
            "responsiblePhone": get_phone_num(),
            "contactPhone": get_phone_num(),
            "oid": oid,
            "provinceAreaId": "2",
            "cityAreaId": "3",
            "areaId": "3",
            "outpatientTimeWeek": "1-7"
        }
        value = request_ryhn().send_value(method, url, data, headers=headers)

    def add_department(self, oid, headers):
        '''
        新增科室
        :return:
        '''
        url = 'https://www.online-medical.com.cn//otsp-manage-sys/japi/v1/orgDept/organization-department-save'
        method = "POST"
        data = {
            "code": str(random.randint(1, 99999)),
            "name": "测试" + str(random.randint(1, 99999)),
            "level": 1,
            "parentId": "0",
            "ctdeptId": random.choice(['25']),
            "responsiblePerson": random.choice(['ryhn', 'eddie', 'peter', 'pluto']) + str(random.randint(1, 999)),
            "introduce": "测试课时",
            "type": 1,
            "oid": oid
        }
        value = request_ryhn().send_value(method, url, data, headers=headers)
        a = re.search("[0-9]+", value.text)
        return a.group()

    def department_service(self, oid, deptId, header):
        '''
        开通科室服务
        :return:
        '''
        url = 'https://www.online-medical.com.cn//otsp-service/japi/v2/manage/dept/service'
        data = [{
                "serviceParamList": [{
                    "serviceId": "12",
                    "status": 1,
                    "activateStatus": 1,
                    "restFlag": 0,
                    "cost": 0
                }, {
                    "serviceId": "14",
                    "status": 1,
                    "activateStatus": 1,
                    "restFlag": 0,
                    "cost": 0
                }],
                "oid": oid,
                "deptId": deptId
                }]
        method = 'POST'
        value = request_ryhn().send_value(method, url, data, headers=header)

    def add_work(self, oid, deptId, header):
        '''
        生成医生信息
        :return:
        '''
        data = {"userParam": {"address": {"detailAddress": "ryhn",
                                          "provinceAreaId": "2",
                                          "cityAreaId": "3",
                                          "countyAreaId": ""},
                              "cardTypeCode": "1",
                              "cardNum": id_card(),
                              "name": random.choice(['刘',
                                                     '关',
                                                     '张',
                                                     '李',
                                                     '王',
                                                     '江',
                                                     '曹',
                                                     '于',
                                                     '童',
                                                     '葛',
                                                     '张',
                                                     '章',
                                                     '孙',
                                                     '刘',
                                                     '马',
                                                     '沈',
                                                     '颜']) + random.choice(['刘',
                                                                            '关',
                                                                            '张',
                                                                            '李',
                                                                            '王',
                                                                            '江',
                                                                            '曹',
                                                                            '于',
                                                                            '童',
                                                                            '葛',
                                                                            '张',
                                                                            '章',
                                                                            '孙',
                                                                            '刘',
                                                                            '马',
                                                                            '沈',
                                                                            '颜']) + random.choice(['刘',
                                                                                                   '关',
                                                                                                   '张',
                                                                                                   '李',
                                                                                                   '王',
                                                                                                   '江',
                                                                                                   '曹',
                                                                                                   '于',
                                                                                                   '童',
                                                                                                   '葛',
                                                                                                   '张',
                                                                                                   '章',
                                                                                                   '孙',
                                                                                                   '刘',
                                                                                                   '马',
                                                                                                   '沈',
                                                                                                   '颜']),
                              "gender": "M",
                              "phone": get_phone_num(),
                              "introduction": "超级厉害的医生hehi额嘿嘿额和IEhi额嘿嘿额和"},
                "medWorkerInfoParam": {"qualificationCode": "",
                                       "practisingCertificateCode": "",
                                       "positionalFirstId": "326",
                                       "positionalSecondId": "387",
                                       "introduce": "",
                                       "speciality": "测试",
                                       "deptId": deptId},
                "oid": oid}
        method = "POST"
        url = 'https://www.online-medical.com.cn//otsp-manage-sys/japi/v1/manage/med-worker'
        value = request_ryhn().send_value(method, url, data, headers=header)
        a = re.search("[0-9]+", value.text)
        return a.group()

    def work_service(self, header, oid, uid):
        '''
        开通医生服务
        :return:
        '''
        url = "https://www.online-medical.com.cn//otsp-service/japi/v2/manage/doctor/service"
        data = [{
                "serviceParamList": [{
                    "serviceId": "12",
                    "status": 1,
                    "activateStatus": 1,
                    "restFlag": 0,
                    "cost": 0
                }, {
                    "serviceId": "14",
                    "status": 1,
                    "activateStatus": 1,
                    "restFlag": 0,
                    "cost": 0
                }],
                "oid": oid,
                "uid": uid
                }]
        method = "POST"
        value = request_ryhn().send_value(method, url, data, headers=header)

    def work_sign(self, oid, deptId, uid, header):
        '''
        医生签约
        :return:
        '''
        data = {
            "jobNumber": str(random.randint(0, 99999)),
            "broleIdList": ["1"],
            "deptId": deptId,
            "oid": oid,
            "uid": uid
        }
        method = "POST"
        url = "https://www.online-medical.com.cn//otsp-manage-sys/japi/v2/manage/doctor/sign"
        value = request_ryhn().send_value(method, url, data, headers=header)

    def sign_log(self, oid, orgendid, header):
        '''
        新增预约途径
        :return:
        '''
        url = "https://www.online-medical.com.cn//otsp-schedule/japi/v1/schedule/appointmentway"
        method = "POST"
        data = {
            "code": "1001",
            "oid": oid,
            "orgEntId": orgendid,
            "name": "测试1",
            "type": "",
            "state": 1,
            "sort": "",
            "advanceDays": "",
            "fristNumAllowTime": "06:09",
            "appApplication": 0,
            "max_res_day": 10,
            "minResDay": 1
        }
        value = request_ryhn().send_value(method, url, data, headers=header)
        a = re.search("[0-9]+", value.text)
        return a.group()

    def shifts_add(self, header, oid):
        '''
        新增班次
        :return:
        '''
        data = {
            "code": "10001",
            "endTime": "23:59",
            "name": "上午",
            "oid": oid,
            "sort": "",
            "startTime": "06:09",
            "state": 1,
            "registerEndTime": "23:59",
            "registerStartTime": "06:09"
        }
        url = 'https://www.online-medical.com.cn//otsp-schedule/japi/v1/schedule/shifts'
        method = 'POST'
        value = request_ryhn().send_value(method, url, data, headers=header)
        a = re.search("[0-9]+", value.text)
        return a.group()

    def schedule_add(
            self,
            datetime1,
            shiftsId,
            orgEntId,
            deptId,
            uid,
            oid,
            header):
        '''
        新增排班
        :return:
        '''
        method = 'POST'
        url = 'https://www.online-medical.com.cn//otsp-schedule/japi/v1/schedule/scheduleInfo-and-scheduletime-insert'
        data = {
            "scheduleInfo": {
                "scheduleDate": datetime1,
                "shiftsId": shiftsId,
                "orgEntId": orgEntId,
                "deptId": deptId,
                "registrationFee": "0.01",
                "registerType": "503",
                "uid": uid,
                "serviceId": "14",
                "totalNums": 50,
                "addNums": 0,
                "appointmentType": 1,
                "visitInterval": 5,
                "visitType": 1,
                "state": 1,
                "reg_code": 0,
                "residual_nums": 0,
                "oid": oid
            },
            "scheduleTimes": [{
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 06:14",
                "nums": 1,
                "sort": 1,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 06:19",
                "nums": 2,
                "sort": 2,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 06:24",
                "nums": 3,
                "sort": 3,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 06:29",
                "nums": 4,
                "sort": 4,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 06:34",
                "nums": 5,
                "sort": 5,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 06:39",
                "nums": 6,
                "sort": 6,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 06:44",
                "nums": 7,
                "sort": 7,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 06:49",
                "nums": 8,
                "sort": 8,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 06:54",
                "nums": 9,
                "sort": 9,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 06:59",
                "nums": 10,
                "sort": 10,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 07:04",
                "nums": 11,
                "sort": 11,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 07:09",
                "nums": 12,
                "sort": 12,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 07:14",
                "nums": 13,
                "sort": 13,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 07:19",
                "nums": 14,
                "sort": 14,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 07:24",
                "nums": 15,
                "sort": 15,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 07:29",
                "nums": 16,
                "sort": 16,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 07:34",
                "nums": 17,
                "sort": 17,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 07:39",
                "nums": 18,
                "sort": 18,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 07:44",
                "nums": 19,
                "sort": 19,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 07:49",
                "nums": 20,
                "sort": 20,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 07:54",
                "nums": 21,
                "sort": 21,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 07:59",
                "nums": 22,
                "sort": 22,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 08:04",
                "nums": 23,
                "sort": 23,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 08:09",
                "nums": 24,
                "sort": 24,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 08:14",
                "nums": 25,
                "sort": 25,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 08:19",
                "nums": 26,
                "sort": 26,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 08:24",
                "nums": 27,
                "sort": 27,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 08:29",
                "nums": 28,
                "sort": 28,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 08:34",
                "nums": 29,
                "sort": 29,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 08:39",
                "nums": 30,
                "sort": 30,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 08:44",
                "nums": 31,
                "sort": 31,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 08:49",
                "nums": 32,
                "sort": 32,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 08:54",
                "nums": 33,
                "sort": 33,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 08:59",
                "nums": 34,
                "sort": 34,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 09:04",
                "nums": 35,
                "sort": 35,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 09:09",
                "nums": 36,
                "sort": 36,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 09:14",
                "nums": 37,
                "sort": 37,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 09:19",
                "nums": 38,
                "sort": 38,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 09:24",
                "nums": 39,
                "sort": 39,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 09:29",
                "nums": 40,
                "sort": 40,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 09:34",
                "nums": 41,
                "sort": 41,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 09:39",
                "nums": 42,
                "sort": 42,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 09:44",
                "nums": 43,
                "sort": 43,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 09:49",
                "nums": 44,
                "sort": 44,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 09:54",
                "nums": 45,
                "sort": 45,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 09:59",
                "nums": 46,
                "sort": 46,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 10:04",
                "nums": 47,
                "sort": 47,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 10:09",
                "nums": 48,
                "sort": 48,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 10:14",
                "nums": 49,
                "sort": 49,
                "state": 1
            }, {
                "appointmentLogId": 0,
                "appointmentState": 0,
                "appointmentWays": "1001",
                "appointmentWaysName": "测试1",
                "isAppointment": 1,
                "numDate": datetime1 + " 10:19",
                "nums": 50,
                "sort": 50,
                "state": 1
            }]
        }
        value = request_ryhn().send_value(method, url, data, headers=header)
        a = re.search("[0-9]+", value.text)
        return a.group()


def create_data():
    '''
    造数据
    :return:
    '''
    a = api_data()
    global head
    head = a.accesstoken()
    reg_code = 20220808080100000000
    mysql = pysql(ip='113.125.48.202',port=40000,name='root',password='jyhkmtyw!1234',database='otsp-schedule')
    for y in range(0, 1):
        # 新增医院并获取oid
        oid = a.add_hospital(head)
        # 开通机构服务
        a.hospital_service(oid, head)
        # 生成二级机构
        a.add_entity(oid, head)
        # 获取实体id
        entity_id = a.get_entity(oid, head)
        # 新增预约途径
        a.sign_log(oid, entity_id, head)
        # 新增班次
        shiftid = a.shifts_add(head, oid)
        for c in range(0, 50):
            print("已经新增了：" + str(c+1) + "科室")
            # 生成科室
            deptid = a.add_department(oid, head)
            # 开通科室服务
            a.department_service(oid, deptid, head)
            for b in range(0, 30):
                print("目前在新增第" + str(b+1) + "医生")
                # 生成医生
                uid = a.add_work(oid, deptid, head)
                # 开通医生服务
                a.work_service(head, oid, uid)
                # 医生签约
                a.work_sign(oid, deptid, uid, head)
                if b == 15:
                    # 更新accesstoken
                    head = a.accesstoken()
                    print("又新增了3000条数据")
                for schedule_num111 in range(1, 51):
                    reg_code = reg_code +1
                    schedule_date = "'"+(datetime.date.today()+datetime.timedelta(days=schedule_num111)).strftime("%Y-%m-%d 00:00:00")+"'"
                    date_time= "'"+datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")+"'"
                    # 新增排班的sql
                    insert_sql1 = """INSERT INTO schedule_info
                    (reg_code,schedule_date,shifts_id,service_id,oid,org_ent_id,dept_id,cdept_id,uid,total_nums,residual_nums,add_nums,registration_fee,appointment_type,state,create_time,update_time,deleted,version,register_type,visit_interval,visit_type,data_sources)
                    VALUES(%s,%s,%s,'14',%s,%s,%s,'25',%s,'50','0','0','0.01','1','1',%s,%s,'0','1','503','5','1','0')
                    """%(str(reg_code),schedule_date,shiftid,oid,entity_id,deptid,uid,date_time,date_time)

                    # 插入新增排班数据
                    mysql.insert(insert_sql1)
                    # 查询接口获取上条插库获得的schedule_id
                    select_sql = """SELECT id FROM schedule_info WHERE reg_code =%s""" %(("'"+str(reg_code)+"'"))

                    a12=mysql.select(select_sql, type='one')['id']
                    for nu in range(1,51):
                        num_date = "'"+(datetime.datetime.now() + datetime.timedelta(minutes=5 * nu)).strftime("%Y-%m-%d %H:%M:%S")+"'"
                        # 插入号子
                        insert_sql2 = """INSERT INTO schedule_time ( schedule_id, nums, sort, num_date, state, appointment_state, appointment_ways, appointment_log_id, is_appointment, create_time, update_time, deleted, version )
                                VALUES(%s,%s,%s,%s,'1','0','1001','0','1',%s,%s,'0','1')
                                """%(a12,nu,nu,num_date,date_time,date_time)

                        mysql.insert(insert_sql2)
    mysql.close()

# if __name__ == "__main__":
    # start = datetime.datetime.now()
    # create_data()
    # # print(start)
    # # thread_1 = threading.Thread(target=create_data())
    # # thread_2 = threading.Thread(target=create_data())
    # # thread_3 = threading.Thread(target=create_data())
    # # thread_4 = threading.Thread(target=create_data())
    # # thread_5 = threading.Thread(target=create_data())
    # # thread_6 = threading.Thread(target=create_data())
    #
    # # # p1 = Process(target=create_data())  # 必须加,号
    # # # p2 = Process(target=create_data())
    # # # p3 = Process(target=create_data())  # 必须加,号
    # # # p4 = Process(target=create_data())
    #
    # # # p1.start()
    # # # p2.start()
    # # # p3.start()
    # # # p4.start()
    #
    # # thread_1.start()
    # # thread_2.start()
    # # thread_3.start()
    # # thread_4.start()
    # # thread_5.start()
    # # thread_6.start()
    #
    # # thread_1.join()
    # # thread_2.join()
    # # thread_3.join()
    # # thread_4.join()
    # # thread_5.join()
    # # thread_6.join()
    # end = datetime.datetime.now()
    # print('time:{time}'.format(time=end - start))
