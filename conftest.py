import logging

import allure
import pytest
from loguru import logger

from utill.request_util.secretykey_util import base641, Sha256
from common.api.requests import request_ryhn
from utill.create_util.yaml_util import yaml_util

#
# @pytest.fixture(scope="session",autouse=True)
# def api_apptoken1():
#     '''
#     通过打开jvm虚拟机，运行jar包来获取apptoken
#     :return:
#     '''
#     with allure.step("获取apptoken"):
#         path = os.getcwd()
#         jarpath = path + r"\demo.jar".format(path)
#         javapath = jpype.getDefaultJVMPath()
#         jpype.startJVM(javapath,
#                        "-ea",
#                        "-Djava.class.path=%s" %jarpath)
#         Jclass = jpype.JClass("com.kt.otsp.base.sdk.AppTokenUtilApi")
#         result = Jclass.apptoken()
#         header = {
#             'apptoken': '{result}'.format(result=result),
#             "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit"
#                       "/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36 Edg/100.0.1185.50",
#              "Content-Type": "application/json;charset=UTF-8"
#         }
#         a = yaml_util().yaml_url(r'\yaml_package\header.yaml')
#         yaml_util().write_yaml(url=a, data=header)
#         jpype.shutdownJVM()
#         return result


@pytest.fixture(scope="session",autouse=True)
def api_apptoken2():
    '''
    通过base64加密和sha256加密组合获取apptoken
    :return:apptoken
    '''
    a = base641(account="otsp-manage-sys-web")
    b = Sha256(secret="f9xcfnqmzv710jf84ft59afc61mpqa8q", key="acfe81hfcn91zmax")
    result = a + '.' + b
    header = {
        'appToken': '{result}'.format(result=result),
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit"
                      "/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36 Edg/100.0.1185.50",
        "Content-Type": "application/json;charset=UTF-8"
    }
    yaml_util().write_yaml(url=yaml_util().yaml_url(r'\yaml_package\header.yaml'), data=header)
    # print(header)


@pytest.fixture(scope="session",autouse=True)
def get_accesstoken(api_apptoken2):
    '''
    获取accesstoken
    :return:
    '''
    with allure.step("获取accessToken"):
        url = r'https://hlwyy.szmbzx.com/test/otsp-manage-sys/japi/v1/manage/login'
        method = 'POST'
        data = {
            "loginName": "jyhk",
            "loginPwd": "Kingtang=2020",
            "manCaptcha": {
                "captchaKey": "string",
                "captchaValue": "string"
            },
            "type": 0
        }
        headers = yaml_util().read_yaml(
            yaml_util().yaml_url(r'\yaml_package\header.yaml'))
        # print(headers)
        value = request_ryhn().send_value(
            method=method, url=url, data=data, headers=headers)
        r = value.json()
        # print(value.text)
        # 重新编写请求头信息
        headers1 = {
            "accessToken": r['accessToken'],
            "Content-Type": headers['Content-Type'],
            "User-Agent": headers['User-Agent'],
            "appToken": headers['appToken']
        }
        yaml_util().write_yaml(yaml_util().yaml_url(
            r'\yaml_package\header.yaml'), data=headers1)


@pytest.fixture(scope="session",autouse=True)
def get_accesstoken2(api_apptoken2):
    '''
    获取用户端的accesstoken
    :return:
    '''
    with allure.step("获取accessToken"):
        url = r'https://hlwyy.szmbzx.com/test/otsp-account/japi/v1/thd-c-account/oauth/token'
        method = 'POST'
        data = {
            "authData": {
                "encryptedData": "15867770302"
                },
            "orgCode": "1526091872196988930",
            "phone":"1587770302",
            "thdAppAid": "olUgE4wBBUgkNkW0AQ57CB-U1Bqw",
            "thdAppId": "wx26103680088996d9",
            "thdAppType": 12,
            "verifyCode": "468564"
            }
        headers = yaml_util().read_yaml(
            yaml_util().yaml_url(r'\yaml_package\header.yaml'))
        headers['appId'] = 'wx26103680088996d9'
        value = request_ryhn().send_value(
            method=method, url=url, data=data, headers=headers)
        r = value.json()

        # 重新编写请求头信息
        headers1 = {
            'appId': 'wx26103680088996d9',
            "accessToken": r['accessToken'],
            "Content-Type": headers['Content-Type'],
            "User-Agent": headers['User-Agent'],
            "appToken": headers['appToken']
        }
        yaml_util().write_yaml(yaml_util().yaml_url(
            r'\yaml_package\header_C.yaml'), data=headers1)
