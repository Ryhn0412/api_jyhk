import json
import os
import traceback
import pandas as pd
from jinja2 import Environment, FileSystemLoader, PackageLoader

from utill.create_util.yaml_util import yaml_util


class excel_util:
    '''
    关于excel的一些操作
    '''

    def excel_url(self, url):
        '''
        获取excel的url路径
        :return:
        '''
        b = 0
        url1 = os.getcwd()
        list1 = url1.partition(r'utill')
        try:
            b = os.path.join(list1[0] + url)
        except BaseException:
            print('获取excel路径失败')
        return str(b)

    def return_url(self, file_url):
        '''
        获取新建file中yaml的url路径
        :return:
        '''
        url1 = os.getcwd()
        try:
            list1 = url1.partition(r'utill')
            b = os.path.join(list1[0] + r'\package\return_message' + file_url)
        except BaseException:
            print('获取新建file内yaml的路径失败')
        return b

    def jira_url(self, url):
        '''
        获取jira中yaml的url路径
        :return:
        '''
        url1 = os.getcwd()
        try:
            list1 = url1.partition(r'utill')
            b = os.path.join(list1[0] + r'\package\jira_message' + url)
        except BaseException:
            print('获取jira_message内yaml的路径失败')
        return b

    def return_build(self, file_url1):
        '''
        获取file的url路径并建立文件夹
        :return:
        '''
        url1 = os.getcwd()
        try:
            list1 = url1.partition(r'utill')
            b = os.path.join(
                list1[0] +
                r'\package\return_message' +
                file_url1)
            os.makedirs(b)
        except BaseException:
            pass
        return b

    def file_build(self, file_url1):
        '''
        获取file的url路径并建立文件夹
        :return:
        '''
        url1 = os.getcwd()
        try:
            list1 = url1.partition(r'utill')
            b = os.path.join(
                list1[0] +
                r'\package\yaml_package' +
                file_url1)
            os.makedirs(b)
        except BaseException:
            pass
        return b

    def file_url(self, file_url):
        '''
        获取新建file中yaml的url路径
        :return:
        '''
        url1 = os.getcwd()
        try:
            list1 = url1.partition(r'utill')
            b = os.path.join(list1[0] + r'\package\yaml_package' + file_url)
        except BaseException:
            print('获取新建file内yaml的路径失败')
        return b

    def del_excel_yaml(self, url1):
        '''
        清空excel生成的yaml数据
        :return:
        '''
        try:
            data = pd.read_excel(
                io=excel_util().excel_url(
                    url=url1),
                sheet_name='Sheet1',
                header=0)
            hang = data.shape[0]
            for z in range(0, hang):
                case1 = {
                    "yaml_url": str(data.values[z][4]) + str(data.values[z][5])
                }
                yaml_util().clear_yaml(
                    url=excel_util().file_url(
                        case1["yaml_url"]))
        except BaseException:
            print("尚未生成测试用例数据，请先注释掉del_excel_yaml这个方法")

    def make_excel_yaml(self, url1, suite_url):
        '''
        生产excel表格数据的yaml测试用例
        :return:
        '''
        data = pd.read_excel(
            io=excel_util().excel_url(
                url=url1),
            sheet_name='Sheet1')
        hang = data.shape[0]
        for x in range(0, hang):
            try:
                case1 = {
                    "Epic": str(data.values[x][0]),
                    "Feature": str(data.values[x][1]),
                    "story": str(data.values[x][2]),
                    "name": str(data.values[x][3]),
                    "yaml_url": str(data.values[x][4]) + str(data.values[x][5]),
                    "request": {
                        "method": str(data.values[x][6]),
                        "url": str(suite_url) + str(data.values[x][7]),
                        "data": json.loads(data.values[x][8])
                    },
                    "response":{
                        "data": json.loads(data.values[x][9])
                    },
                    "test_url": {
                        "test_url": str(data.values[x][11]),
                        "test_name": str(data.values[x][12])
                    },
                    "exceptdata": {
                        "except_data": json.loads(data.values[x][10]),
                        "assignee": str(data.values[x][13])
                    }
                }
                suitcase = list()
                suitcase.append(case1)
                excel_util().file_build(
                    file_url1=str(
                        data.values[x][4]))
                yaml_util().write_excel_yaml(
                    url=excel_util().file_url(
                        case1['yaml_url']), data=suitcase)
                suitcase.clear()
            except BaseException:
                print("第{num}条测试数据，JSON解析出错".format(num=x + 2))

