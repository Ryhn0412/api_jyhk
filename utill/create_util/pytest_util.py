import os
import traceback

import pandas as pd
from jinja2 import PackageLoader, Environment

from utill.create_util.excel_util import excel_util


def make_pytest(url1):
    '''
    通过jinjia2生成pytest文件
    '''
    data = pd.read_excel(
        io=excel_util().excel_url(
            url=url1),
        sheet_name='Sheet1')
    hang = data.shape[0]
    # 处理数据
    url1 = os.getcwd()
    list1 = url1.partition(r'utill')
    # 读取表格数据
    for x in range(0, hang):
        try:
            # 生成pytest文件
            # 调用jinjia类（使用包模板参数）
            env = Environment(
                loader=PackageLoader(
                    "ryhn_template",
                    "templates"))
            # 定义指定模板名
            if str(data.values[x][1]) =='用户端':
                template = env.get_template("user_config.j2")
            elif str(data.values[x][1]) == '医生端':
                template = env.get_template("doctor_config.j2")
            elif str(data.values[x][1]) =='后台管理':
                template = env.get_template("base_config.j2")
            # 进行模板渲染
            content = template.render(
                epic=str(data.values[x][0]),
                feature=str(data.values[x][1]),
                class_name=str('Test_') + (str(data.values[x][12]).split('/', 1)[1]).split('.',1)[0],
                story=str(data.values[x][2]),
                yaml_url='/yaml_package' + str(data.values[x][4]) + str(data.values[x][5]),
                test_name=str('test_') + (str(data.values[x][12]).split('/', 1)[1]).split('.',1)[0],
                caseinfo_name=str(data.values[x][3]))
            a13 = str(data.values[x][11])
            a14 = str('Test_') + (str(data.values[x][12]).split('/', 1)[1]).split('.',1)[0]
            pytest_url = 'Suites' + \
                          a13 + '/' + a14 + '.py'
            # 生成pytest文件所属文件夹
            try:
                b = os.path.join(
                    list1[0] +
                    '/Suites' + str(data.values[x][11]))
                os.makedirs(b)
                # 获取每层文件夹必须要的__init__.py文件
                make_init(data=str(data.values[x][11]))
            except BaseException:
                pass
            # 生成pytest文件
            file_url = os.path.join(list1[0] + '/' + pytest_url)
            if os.path.isfile(file_url):
                # 如果已存在该文件，则不重写
                pass
            else:
                # 不存在则写入
                with open('{pytest_url}'.format(pytest_url=file_url), 'w+', encoding='UTF-8') as fp:
                    fp.write(content)
        except BaseException:
            print(traceback.print_exc())

def make_init(data):
    '''
    关于生成init.py文件
    '''
    s = data.count('/')
    a = data.split('/', s)
    b = '/__init__.py'
    c = '/Suites'
    url1 = os.getcwd()
    list2 = url1.partition(r'utill')
    b1 = list2[0] + c + '/' + a[1] + b
    b2 = list2[0] + c + '/' + a[1] + '/' + a[2] + b
    # b3 = list2[0] + c + '/' + a[1] + '/' + a[2] + '/' + a[3] + b
    list1 = [b1,b2]
    for y in range(0,s):
        make_py(pytest_url=list1[y])

def make_py(pytest_url):
    # 生成__init__.py文件
    with open('{pytest_url}'.format(pytest_url=pytest_url), 'w', encoding='UTF-8') as fp:
        a = '#小哥哥小姐姐们，这个文件不能删除哈！'
        fp.write(a)
