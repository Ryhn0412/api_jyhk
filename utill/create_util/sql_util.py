import pymysql


class pysql(object):
    '''
    建立关于数据库的工具类
    '''
    def __init__(self,ip='127.0.0.1',port=3306,name='root',password='xuehaoye0412',database='otsp-scheddule',charset='utf8'):
        '''
        定义数据库相关配置信息
        '''
        try:
            self.coon =pymysql.connect(host=ip,port=port,user=name,password=password,database=database,charset=charset)
            self.cursor = self.coon.cursor(cursor=pymysql.cursors.DictCursor)
        except Exception as e:
            print(e)

    def delete(self,delete_sql):
        '''
        关于数据库删除操作
        :return:返回插入后该数据的Id
        '''
        sql = delete_sql
        try:
            self.coon.ping(reconnect=True)
            self.cursor.execute(sql)
            self.coon.commit()
        except Exception as e:
            print(e)
            # 若发生错误，则回滚
            self.coon.rollback()

    def update(self,update_sql):
        '''
        关于数据库删除操作
        :return:返回插入后该数据的Id
        '''
        sql = update_sql
        try:
            self.coon.ping(reconnect=True)
            self.cursor.execute(sql)
            self.coon.commit()
        except Exception as e:
            print(e)
            # 若发生错误，则回滚
            self.coon.rollback()


    def insert(self, insert_sql):
        '''
        关于数据库插入操作
        :return:返回插入后该数据的Id
        '''
        sql = insert_sql
        try:
            self.coon.ping(reconnect=True)
            self.cursor.execute(sql)
            self.coon.commit()
            # 读取新插入语句的id
            self.data_insert = self.cursor.lastrowid
        except Exception as e:
            print(e)
            # 若发生错误，则回滚
            self.coon.rollback()
        return self.data_insert



    def select(self, select_sql, type):
        '''
        关于数据库查找操作
        :return:返回查找的数据(返回格式为tuple)
        '''
        try:
            self.coon.ping(reconnect=True)
            self.cursor.execute(select_sql)
            if type == 'one':
                self.select_data = self.cursor.fetchone()
                self.coon.commit()
            else:
                self.select_data = self.cursor.fetchall()
                self.coon.commit()
        except Exception as e:
            print(e)
            # 若发生错误，则回滚
            self.coon.rollback()
        return self.select_data

    def close(self):
        '''
        关闭数据库连接
        :return:
        '''
        self.cursor.close()
        self.coon.close()


if __name__=='__main__':
    mysql = pysql(database='otsp_scheddule')
    # select_sql = """SELECT id FROM schedule_info WHERE reg_code = %s""" % ('20210329102334000010')
    # a1 = mysql.select(select_sql,type='one')
    # insert_sql ="""INSERT INTO schedule_info (reg_code,schedule_date,shifts_id,service_id,oid,org_ent_id,dept_id,cdept_id,uid,total_nums,residual_nums,add_nums,registration_fee,appointment_type,state,create_time,update_time,deleted,version,register_type,visit_interval,visit_type,data_sources)
    #                 VALUES('20220803151632037687','2024-08-03 00:00:00','1552174182559617025','14','1552174180827369474','134','1315104008062443800','25','1552180247334457345','50','0','0','0.01','1','1','2022-08-03 14:35:33','2022-08-03 14:35:33','0','1','503','5','1','0')
	# """
    # a1 = mysql.insert(insert_sql)
    #
    # mysql.close()
