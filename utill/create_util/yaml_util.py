
import os
import yaml


class yaml_util:
    '''
    关于yaml文件的一些操作，主要包括读取、写入和
    获取yaml路径（获取yaml该方法只能在Suites文件夹下运用）
    '''

    def yaml_data(self,url):
        '''
        获取指定路径下yaml内的数据(仅能在Suites文件夹下使用）
        '''
        caseinfo3 = yaml_util().read_yaml(yaml_util().yaml_url(
            url=url))
        return caseinfo3

    def yaml_url(self, url):
        '''
        获取yaml的url路径(仅能在Suites文件夹下使用）
        :return:
        '''
        url1 = os.getcwd()
        try:
            list1 = url1.partition(r'Suites')
            b = os.path.join(list1[0] + r'\package' + url)
        except BaseException:
            print('获取yaml路径失败')
        return b

    def write_yaml(self, url, data):
        '''
        写入数据于yaml文件中
        :return:
        '''
        with open(url, encoding='utf-8', mode='w+') as f:
            yaml.dump(data, f, encoding='utf-8', allow_unicode=True)
            f.close()

    def read_yaml(self, url):
        '''
        读取数据从yaml文件中
        :return:
        '''
        with open(url, encoding='utf-8', mode='r+') as f:
            value = yaml.unsafe_load(f.read())
        return value

    def clear_yaml(self, url):
        '''
        清除数据从yaml文件中
        :param url: yaml路径地址
        :return:
        '''
        with open(url, encoding='utf-8', mode='w+') as f:
            f.truncate()

    def write_excel_yaml(self, url, data):
        '''
        写入数据于yaml文件中
        :return:
        '''
        with open(url, encoding='utf-8', mode='a+') as f:
            yaml.dump(data, f, encoding='utf-8', allow_unicode=True)
            f.close()
