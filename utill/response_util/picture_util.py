import os

from jira import JIRA
import matplotlib.pyplot as plt

class jira_util(object):
    '''
    关于Jira的工具类
    '''
    def __init__(self, url='http://10.1.50.165:8080/', login=('xuehaoye','xuehaoye0412')):
        self.jira = JIRA(server=url,basic_auth=login)
        # self.project = project

    def data_list(self, issue_key):
        issue = self.jira.issue(issue_key, fields='summary,description,comment,assignee,project,status')
        dict = {"summary":issue.fields.summary,
                "project":issue.fields.project.name,
                "status":issue.fields.status,
                "assignee":issue.fields.assignee}
        return dict


    def make_picture(self, name1, a2,title):
        fig, ax = plt.subplots(figsize=(10, 7))

        ax.bar(x=name1, height=a2)
        ax.set_title("{title}".format(title=title), fontsize=15)
        plt.rcParams["font.sans-serif"] = ["SimHei"]
        url = os.getcwd()
        plt.savefig(url+r"\{title}.jpg".format(title=title))
        # plt.show()

    def make_data(self):
        project1 = self.jira.search_issues('created >= -30d AND text ~ "测试发现"',
                                         fields='summary,description,comment,status', maxResults=-1)
        res = [project1[a].key for a in range(0, len(project1))]
        # 获取问题的key值
        # 获取问题的详情
        rea = [self.data_list(b) for b in res]
        # 获取各个问题的处理人
        reb = [rea[c]['assignee'] for c in range(0, len(rea))]
        # 获取问题的处理人（去重后）
        name = [reb[d].displayName for d in range(0, len(reb))]
        name1 = list(set(name))

        a2 = list()
        na = list()
        nu = 0
        mun = list()
        for i in range(0, len(name1)):
            # 获取这个人本月有多少bug
            p = name.count(name1[i])
            na.append(name1[i])
            a2.append(int(p))
            for y in range(0,len(rea)):
                # print(rea[y]['status'])
                status_made = ['处理完成','完成关闭','已完成','任务已关闭']
                if str(rea[y]['assignee']) == name1[i] and str(rea[y]['status']) in status_made:
                    nu = nu + 1
            if nu == 0:
                # print(nu)
                mun.append(str(0))
            else:
                od = '{mult}'.format(mult=round(nu/p,2))
                mun.append(od)
            nu = 0
        abc = dict(zip(name1,mun))
        abd = sorted(abc.items(), key=lambda item:item[1])
        name1 = [abd[x][0] for x in range(0,len(abd))]
        mun = [abd[g][1] for g in range(0,len(abd))]
        self.make_picture(name1,mun,title='各人员BUG修复率')
        abc = dict(zip(na, a2))
        abd = sorted(abc.items(), key=lambda item: item[1])
        name1 = [abd[x][0] for x in range(0, len(abd))]
        a2 = [abd[g][1] for g in range(0, len(abd))]
        self.make_picture(name1,a2,title='近30天测试发现的BUG')

if __name__=='__main__':
    a1 = jira_util()
    a1.make_data()