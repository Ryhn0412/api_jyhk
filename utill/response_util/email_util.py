import datetime
import logging
import os
import smtplib
import traceback
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.header import Header

from loguru import logger

from utill.create_util.yaml_util import yaml_util
from utill.response_util.picture_util import jira_util

class email():
    '''
    关于发生邮箱的工具定义类
    '''
    def __init__(self):
        url = os.getcwd()
        url = url.partition(r'utill')
        b = os.path.join(url[0] + r'\package\emaill_message\base_message.yaml')
        casedata = yaml_util().read_yaml(b)
        self.username = casedata['username']
        self.password = casedata['password']
        self.sender = casedata['sender']
        self.receivers = casedata['receivers']

    def image_config(self, url, name, msgRoot):
        '''
        附加图片
        :return:
        '''
        # 指定图片为当前目录
        with open(url, 'rb') as fp:
            msgImage = MIMEImage(fp.read())
            # 定义图片 ID，在 HTML 文本中引用
            msgImage.add_header('Content-ID', name)
            msgRoot.attach(msgImage)

    def msgRoot_config(self,Fromname,Toname,subject):
        '''
        邮件属性配置
        :return:
        '''
        msgRoot = MIMEMultipart('related')
        msgRoot['From'] = Header(Fromname, 'utf-8')
        msgRoot['To'] = Header(Toname, 'utf-8')
        msgRoot['Subject'] = Header(subject, 'utf-8')

        msgAlternative = MIMEMultipart('alternative')
        msgRoot.attach(msgAlternative)

        mail_msg = """
                       <p>近30天BUG合计：</p>
                       <p><img src="cid:image1"></p>
                       <p>各开发修复情况统计：</p>
                       <p><img src="cid:image2"></p>
                       """
        msgAlternative.attach(MIMEText(mail_msg, 'html', 'utf-8'))
        self.image_config(url=os.getcwd() + r'\近30天测试发现的BUG.jpg',name='<image1>',msgRoot=msgRoot)
        self.image_config(url=os.getcwd() + r'\各人员BUG修复率.jpg',name='<image2>',msgRoot=msgRoot)
        return msgRoot.as_string()

    def send_email(self,url='smtp.exmail.qq.com',port=465):
        '''
        邮件发送
        :param url: 发生邮箱服务器
        :param port: 端口号
        :return:
        '''
        try:
            smtpObj = smtplib.SMTP_SSL(url,port)
            smtpObj.login(self.username, self.password)
            smtpObj.sendmail(self.sender, self.receivers, self.msgRoot_config(Fromname='测试组',Toname='各位大佬',subject='近30天测试发现的BUG统计'))
            logger.info("邮件发送成功")
        except smtplib.SMTPException:
            logger.error("Error: 无法发送邮件")
            logger.error(traceback.print_exc())


def email_send():
    now = datetime.date.today().strftime('%d')
    datelist = ['1', '2', '3']
    if now in datelist:
        a1 = jira_util()
        a1.make_data()
        email().send_email()
    else:
        logger.info("未到月初，不统计bug情况")


if __name__=='__main__':
    email_send()






# def email_util():
#     # 获取配置文件信息
#     url = os.getcwd()
#     url = url.partition(r'utill')
#     b = os.path.join(url[0] + r'\package\emaill_message\base_message.yaml')
#     casedata = yaml_util().read_yaml(b)
#     # 填入基本信息
#     username = casedata['username']
#     password = casedata['password']
#     sender = casedata['sender']
#     receivers = casedata['receivers']
#     # 接收邮件，可设置为你的QQ邮箱或者其他邮箱
#
#     msgRoot = MIMEMultipart('related')
#     msgRoot['From'] = Header("测试组", 'utf-8')
#     msgRoot['To'] = Header("各位大佬", 'utf-8')
#     subject = '【BUG统计】————近30天情况合计'
#     msgRoot['Subject'] = Header(subject, 'utf-8')
#
#     msgAlternative = MIMEMultipart('alternative')
#     msgRoot.attach(msgAlternative)
#
#     mail_msg = """
#     <p>近30天BUG合计：</p>
#     <p><img src="cid:image1"></p>
#     <p>各开发修复情况统计：</p>
#     <p><img src="cid:image2"></p>
#     """
#     msgAlternative.attach(MIMEText(mail_msg, 'html', 'utf-8'))
#     url = os.getcwd()
#     # 指定图片为当前目录
#     fp = open(url+r'\近30天测试发现的BUG.jpg', 'rb')
#     msgImage = MIMEImage(fp.read())
#     fp.close()
#
#     # 定义图片 ID，在 HTML 文本中引用
#     msgImage.add_header('Content-ID', '<image1>')
#     msgRoot.attach(msgImage)
#
#     fp = open(url +r'\各人员BUG修复率.jpg', 'rb')
#     msgImage = MIMEImage(fp.read())
#     fp.close()
#
#     # 定义图片 ID，在 HTML 文本中引用
#     msgImage.add_header('Content-ID', '<image2>')
#     msgRoot.attach(msgImage)
#     try:
#         smtpObj = smtplib.SMTP_SSL('smtp.exmail.qq.com',465)
#         smtpObj.login(username,password)
#         smtpObj.sendmail(sender, receivers, msgRoot.as_string())
#         logger.info("邮件发送成功")
#     except smtplib.SMTPException:
#         logger.error("Error: 无法发送邮件")
#         logger.error(traceback.print_exc())
