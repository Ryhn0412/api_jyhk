import logging
from loguru import logger
from utill.response_util.jira_util import jira_util


class MyLog:
    def my_log(self, msg, name):

        my_logger = logging.getLogger('{name}'.format(name=name))

        my_logger.setLevel('DEBUG')

        ch = logging.StreamHandler()
        ch.setLevel('DEBUG')

        fmt = '------------------------------------%(levelname)s:%(asctime)s--%(filename)s------------------------------------\n' \
              '测试对象：%(name)s\n测试详情：\n\t%(message)s'
        formatter = logging.Formatter(fmt=fmt)

        fh = logging.FileHandler('ryhn_log.log', encoding='utf-8', mode='a')
        fh.setLevel('DEBUG')
        fh.setFormatter(formatter)
        ch.setFormatter(formatter)

        my_logger.addHandler(ch)
        my_logger.addHandler(fh)

        my_logger.warning(msg)

        my_logger.removeHandler(ch)
        my_logger.removeHandler(fh)

    def request_log(self, caseinfo, value, data):
        html = '请求体:\n\t\t' \
               '请求方式: {a2}\n\t\t' \
               '请求地址ַ:{a1}\n\t\t' \
               '请求参数：{a3}\n\t' \
               '响应体:\n\t\t' \
               '响应状态码:{b2}\n\t\t' \
               '响应参数：{b3}\n\t' .format(a1=caseinfo['request']['url'], a2=caseinfo['request']['method'],
                                           a3=data,b2=value.status_code,b3=value.text)
        MyLog().my_log(html, name=caseinfo['name'])
        jira_util().create_error(value=html,
                                 caseinfo=caseinfo)
