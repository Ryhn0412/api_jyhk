import json
import re

import jsonpath
from deepdiff import grep


class json_util:
    '''
    关于jsonpath的工具类
    '''
    def jsonpath(self, value, path):
        '''
        用jsonpath进行数据提取
        :return:
        '''
        json_data = jsonpath.jsonpath(json.loads(json.dumps(value)),path)
        return json_data
    def deepget(self, except_code, really_code):
        '''
        用deepdiff进行查找，再通过正则提取结果所在的列数
        :return:
        '''
        a = really_code | grep(except_code)
        b = re.search(r"([)\d+(])", str(a['matched_values']), re.S)
        c = int(b.group())
        return c