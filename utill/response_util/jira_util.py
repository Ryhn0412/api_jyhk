import traceback
from operator import length_hint

import allure
import pytest
from loguru import logger

from utill.response_util.add_utill import add_util
from jira import JIRA
from utill.create_util.excel_util import excel_util
from utill.create_util.yaml_util import yaml_util

class jira_util(add_util):
    '''
    关于在jira平台上进行增删的功能操作
    '''
    def add_issue(self):
        # 前置操作;登陆jira账号
        base_login = yaml_util().read_yaml(url=excel_util().jira_url(
            url=r'/base_message/base_login.yaml'
        ))
        jira = JIRA(
            server='{url}'.format(
                url=base_login['url']), basic_auth=(
                '{login_name}'.format(
                    login_name=base_login['login_name']), '{login_serect}'.format(
                    login_serect=base_login['login_secret'])))

        # 获取此次自动化测试出错用例的yaml文件
        caseinfo = yaml_util().read_yaml(url=excel_util().jira_url(
            url=r'/error_message/error_log.yaml'
        ))
        pj_id = jira.project(
            '{project_key}'.format(
                project_key=base_login['project_key'])).id

        issue_list = list()
        # 判断是否重复
        for x in range(0, length_hint(caseinfo)):
            # 查找是否有重复问题提交
            issues = jira.search_issues(
                jql_str=r'project = {a} AND summary ~ {name}'.format(
                        a=pj_id, name=caseinfo[x]['summary']), fields='summary')
            if issues == []:
                # 如果没重复就创建
                issue_dict = {
                    'project': {'id': pj_id},  # 项目id
                    'summary': caseinfo[x]['summary'],  # BUG概要
                    'description': caseinfo[x]['description'],  # BUG详情
                    'priority': {'name': 'High'},  # bug优先级
                    'labels': ['聚医蕙康'],
                    'parent': {'key': base_login['issue_key']},  # 所属的父问题集
                    'assignee': {'name': caseinfo[x]['assignee']},  # 指派人名字全拼
                    'issuetype': {'id': 10400}  # 子问题的id
                }
                issue_list.append(issue_dict)
            else:
                # 如果重复的话，先判断该问题是否关闭，如果关闭就重启，未关闭就跳过
                issue1 = jira.issue('{a}'.format(a=issues[0]))
                if jira.transitions(issues[0])[0]['name'] == '重启':
                    # 重启该问题
                    jira.transition_issue(issue1, '重启')
                    issue1.update(description='{name}'.format(name=caseinfo[x]['description']))
                else:
                    pass
        try:
            #   批量提交问题集
            iss = jira.create_issues(issue_list)
            logger.info("已在jira上提交本次自动化发现的Bug")
        except:
            # print(issue_list)
            # print("提交完成，未有新BUG出现")
            logger.info("提交完成，未有新BUG出现")
            yaml_util().clear_yaml(
                url=excel_util().jira_url(r'/error_message/error_log.yaml')
            )
        yaml_util().clear_yaml(
            url=excel_util().jira_url(r'/error_message/error_log.yaml')
        )

    def create_error(self, caseinfo, value):
        '''
        用例出错时，用做保存保存信息
        :return:
        '''
        with allure.step("bug登记至jira"):
            listcase = [{
                'summary': '自动化测试发现：'+'{a}'.format(
                    a=caseinfo['name']),
                'description': value,
                'assignee': '{c}'.format(
                    c=caseinfo['exceptdata']['assignee'])}]
            yaml_util().write_excel_yaml(
                url=yaml_util().yaml_url(
                    url=r'\jira_message\error_message\error_log.yaml'),
                data=listcase)
            pytest.xfail(reason='断言失败')
