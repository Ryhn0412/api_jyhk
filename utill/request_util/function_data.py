import datetime
import random

from utill.random_util.random_phone import get_phone_num


def datetime_today():
    '''返回今天年月日的数值'''
    return datetime.datetime.now().strftime("%Y-%m-%d")
def datetime_tomorrow():
    """返回明天的年月日数值"""
    return (datetime.datetime.now()+datetime.timedelta(days=1)).strftime("%Y-%m-%d")
def datetime_twodays():
    """返回两天后的年月日数值"""
    return (datetime.datetime.now()+datetime.timedelta(days=2)).strftime("%Y-%m-%d")
def datetime_threedays():
    """返回三天后的年月日数值"""
    return (datetime.datetime.now()+datetime.timedelta(days=3)).strftime("%Y-%m-%d")
def datetime_sevendays():
    """返回七天后的年月日数值"""
    return (datetime.datetime.now()+datetime.timedelta(days=7)).strftime("%Y-%m-%d")
def datetime_now():
    '''返回当前时间'''
    return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
def datetime_tenminutes():
    '''返回十分钟后的时间'''
    return (datetime.datetime.now()+ datetime.timedelta(minutes=10)).strftime("%Y-%m-%d %H:%M:%S")
def random_code():
    '''返回一个随机生成的8位code值'''
    return str(random.randint(10000000,99999999))
def phone():
    '''返回一个手机号'''
    return get_phone_num()
def id_card():
    '''返回一个身份证号'''
    return id_card()
def random_name():
    '''返回一个随机姓名'''
    return random.choice(['刘','关','张','李','王','江','曹','于','童','葛','张', '章','孙','刘','马', '沈','颜']) +\
            random.choice(['刘', '关', '张', '李', '王', '江', '曹', '于', '童', '葛', '张', '章', '孙', '刘', '马', '沈', '颜'])+\
                random.choice(['刘', '关', '张', '李', '王', '江', '曹', '于', '童', '葛', '张', '章', '孙', '刘', '马', '沈', '颜'])