import base64
import hashlib
import hmac
import json
import time

def base641(account):
    # 自定义一个时间戳，类似与java的System.currentTimeMillis()方法
    timestamp = lambda: int(round((time.time()-43200)*1000))
    a1 = " {\"appId\":\"" + account + "\",\"timestamp\":" + str(timestamp()) + "}"
    strEncode = base64.b64encode(a1.encode('utf8'))
    return strEncode.decode('utf-8')

def Sha256(secret, key):
    '''
    HMAC SHA256加密方式
    '''
    # 获取当前计算机时间
    timestamp = lambda: int(round((time.time()-43200) * 1000))
    preSign = secret + "." + str(timestamp())
    # 翻译成字节
    preSign = preSign.encode('utf-8')
    signature = hmac.new(key=key.encode('utf-8'), msg=preSign, digestmod='sha256').hexdigest()
    return str(signature)

def MD5(string):
    """
    MD5加密
    """
    h1 = hashlib.md5()    # 创建对象
    # 声明encode为utf-8
    h1.update(string.encode(encoding = 'utf-8'))
    # 返回加密后的字符串
    return h1.hexdigest()
if __name__ == '__main__':
    a = base641(account="otsp-manage-sys-web")
    b = Sha256(secret="f9xcfnqmzv710jf84ft59afc61mpqa8q",key="acfe81hfcn91zmax")
    print(a + '.' +b)