import os
import jinja2
import yaml
import importlib
import inspect
from utill.create_util.yaml_util import yaml_util


def render(tpl_path, **kwargs):
    """渲染yml文件"""
    path, filename = os.path.split(tpl_path)
    return jinja2.Environment(loader=jinja2.FileSystemLoader(path or './')
    ).get_template(filename).render(**kwargs)

def all_functions():
    """加载function_data.py模块"""
    debug_module = importlib.import_module(".request_util.function_data",package='utill')
    all_function = inspect.getmembers(debug_module, inspect.isfunction)
    return dict(all_function)

def yaml_data(ym_url):
    """实现yaml替换驱动"""
    # 获取yaml路径
    yaml_url = os.getcwd().split('Suites')[0] + r'\package' + ym_url
    r = render(yaml_url, **all_functions())
    yaml_util().write_yaml(yaml_url, yaml.safe_load(r))
