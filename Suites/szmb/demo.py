import os

import yaml

from utill.create_util.yaml_util import yaml_util
from utill.request_util.yaml_data import render, all_functions

if __name__=='__main__':
    # 获取yaml路径
    yaml_url = os.getcwd().split('Suites')[0] + r'\package' + r'/yaml_package/demo.yaml'
    print(yaml_url)
    r = render(yaml_url, **all_functions())
    yaml_util().write_yaml(yaml_url, yaml.safe_load(r))