import datetime
import random
import traceback

import allure
import pytest
from loguru import logger
from common.api.doubule_requests import double_requests
from utill.request_util.header_util import header_util
from utill.response_util.assert_util import assert_ryhn
from utill.create_util.yaml_util import yaml_util
from utill.response_util.logger_util import MyLog

# -*- author: Ryhn -*-
# -*- encoding: utf-8 -*-
@allure.epic("深圳慢病")
@allure.feature("后台管理")
class Test_hospital_service:
    '''
    关于后台管理接口类测试
    '''
    # @logger.catch
    # @pytest.mark.xdist_group("group1")
    @allure.story("基础资源配置")
    @allure.title("{caseinfo[name]}")
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/szmb/basic_config/hospital_service.yaml'))
    @allure.severity(allure.severity_level.CRITICAL)
    def test_hospital_service(self, caseinfo):
        '''
        基础资源配置
        :return: 测试结果
        '''
         # 入参处理
        data = double_requests().get_data(caseinfo)
        if caseinfo['name'] =='基础资源配置——新增医院':
            data['code'] = str(random.randint(2000,999999999))
        elif caseinfo['name'] =='基础资源配置——获取指定的排班列表':
            data['fromDate'] =(datetime.datetime.now() + datetime.timedelta(days=1)).strftime("%Y-%m-%d")
            data['toDate'] = (datetime.datetime.now() + datetime.timedelta(days=2)).strftime("%Y-%m-%d")
        elif caseinfo['name'] =='基础资源配置——新增排班' or caseinfo['name'] =='基础资源配置——编辑排班信息':
            data['scheduleInfo']['scheduleDate'] =(datetime.datetime.now() + datetime.timedelta(days=1)).strftime("%Y-%m-%d")
            data['scheduleTimes'][0]['numDate'] =(datetime.datetime.now() + datetime.timedelta(days=1)).strftime("%Y-%m-%d %H:%M:%S")
            data['scheduleTimes'][1]['numDate'] = (datetime.datetime.now() + datetime.timedelta(days=1) + datetime.timedelta(minutes=10)).strftime("%Y-%m-%d %H:%M:%S")
        elif caseinfo['name'] =='基础资源配置——发布排班' or caseinfo['name'] =='基础资源配置——停诊排班' or caseinfo['name'] =='基础资源配置——废弃排班':
            schedule = data['id']
            data = [schedule]
        # 发起请求
        if caseinfo['name'] =='基础资源配置——删除指定科室' or caseinfo['name'] =='基础资源配置——删除指定二级机构' or caseinfo['name'] =='基础资源配置——删除指定医院' :
            ido = data['id']
            url = str(caseinfo['request']['url'] +"?id="+ido)
            value = double_requests().send_value(method="POST",url=url,data=None,headers=header_util().change_header(select='ax'))
        else:
            value = double_requests().ryhn_request(caseinfo=caseinfo, data=data)
        with allure.step("返回请求结果"):
            allure.attach(
                body="{response}".format(response=value.text),
                name="返回数据:",
                attachment_type=allure.attachment_type.TEXT)
        try:
            # 进行接口断言
            assert_ryhn().assertEqual(200, value.status_code)
            assert_ryhn().exist(except_code=caseinfo['exceptdata']['except_data']['message'],
                                really_code=value.text)
            # 进行存储接口关联的变量
            double_requests().complex_data(caseinfo, value)
            # 打印请求成功的日志
            logger.info(str(caseinfo['name']) + '结果:请求正常')
        except:
            # print(traceback.print_exc())
            #   断言失败，则调用该语句创建日志（用于上传至jira提交bug）
            MyLog().request_log(caseinfo=caseinfo, data=data, value=value)