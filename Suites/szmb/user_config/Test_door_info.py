import random
import allure
import pytest
from loguru import logger
from common.api.doubule_requests import double_requests
from utill.response_util.assert_util import assert_ryhn
from utill.create_util.yaml_util import yaml_util
from utill.response_util.logger_util import MyLog

# -*- author: Ryhn -*-
# -*- encoding: utf-8 -*-
@allure.epic("深圳慢病")
@allure.feature("用户端")
class Test_door_info:
    '''
    关于用户端接口类测试
    '''
    @allure.story("门户页面展示")
    @allure.title("{caseinfo[name]}")
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/szmb/user_config/door_info.yaml'))
    @allure.severity(allure.severity_level.CRITICAL)
    def test_door_info(self, caseinfo):
        '''
        基础资源配置——获取用户信息
        :return: 测试结果
        '''
         # 入参处理
        data = double_requests().get_data(caseinfo)
        # 发起请求
        value = double_requests().ryhn_request_C(caseinfo=caseinfo, data=data)
        with allure.step("返回请求结果"):
            allure.attach(
                body="{response}".format(response=value.text),
                name="返回数据:",
                attachment_type=allure.attachment_type.TEXT)
        try:
            # 进行接口断言
            assert_ryhn().assertEqual(200, value.status_code)
            assert_ryhn().exist(except_code=caseinfo['exceptdata']['except_data']['message'],
                                really_code=value.text)
            # 打印请求成功的日志
            logger.info(str(caseinfo['name']) + '结果:请求正常')
            # 进行存储接口关联的变量
            double_requests().complex_data(caseinfo, value)
        except:
            # print(traceback.print_exc())
            #   断言失败，则调用该语句创建日志（用于上传至jira提交bug）
            MyLog().request_log(caseinfo=caseinfo, data=data, value=value)